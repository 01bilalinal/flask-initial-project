# Flask Initial Project

This project is an example Flask factory project.

## Installation

Firstly, Docker must be installed for this application to work.

1. Clone this repository: `git clone https://gitlab.com/01bilalinal/flask-initial-project.git`
2. Go to the cloned directory: `cd flask-initial-project`
3. Copy the environment example file: `cp .env.example .env`
4. Start the application using Docker Compose: `docker-compose up -d`
5. Go to `http://localhost:5000/` in your browser.

## Example Usage

Once the project is running, you can use the example API by following these steps:

#### Creating all Tables
To create all tables in the project, you can follow these steps:

1. Open your terminal or command prompt.
2. Use the following command to enter the Docker container running the Python environment: `docker exec -it python bash`
    
    This command will provide you with an interactive terminal session inside the Docker container named "python".
3. Once you are inside the container, run the following command to execute the Flask command-line interface: `flask create-tables`

    This command will create all the required tables in your database based on the defined models in your Flask project.


#### Creating a User

To create a user, you can use the following curl command:

```shell
curl --location 'http://127.0.0.1:5000/create' \
--header 'Content-Type: application/json' \
--data '{
    "username": "joedoe",
    "first_name": "Joe",
    "last_name": "Doe"
}'
```

The above command will create a user with the username "joedoe" and the name "Joe Doe".

#### Searching for a User

To search for a specific user, you can use the following curl command:

```shell
curl --location --request POST 'http://127.0.0.1:5000/find/joedoe' \
--header 'Content-Type: application/json'
```