import os

from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS

from registerations.register_commands import register_commands
from registerations.register_extensions import register_extensions
from registerations.register_response_time import register_request
from src.service.exception_handler import ExceptionHandler


def create_app():
    load_dotenv()

    app = Flask('flask-initial-project', static_folder='public')
    app.url_map.strict_slashes = False

    CORS(app)
    app.secret_key = os.getenv('APP_SECRET_KEY')

    from src.router.blueprint import Blueprint

    bp = Blueprint()
    app.register_blueprint(bp.api_routes(), url_prefix='/')
    app.register_error_handler(Exception, ExceptionHandler())

    register_extensions(app)
    register_commands(app)
    register_request(app)

    return app


if __name__ == '__main__':
    create_app().run(host='0.0.0.0', port=8080, debug=True)
