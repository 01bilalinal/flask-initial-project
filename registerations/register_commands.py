def register_commands(app):
    from src import command

    app.cli.add_command(command.AutoMigrateCommand.execute)
