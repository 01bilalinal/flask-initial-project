import os


def register_extensions(app):
    register_sqlalchemy(app)


def register_sqlalchemy(app):
    from extension.sqlalchemy import db
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URL')

    db.init_app(app)
