import time
from datetime import datetime

from flask import g, current_app, request

from src.helper.time_helper import TimeHelper


def register_request(app):
    @app.before_request
    def before_request():
        g.start_time = time.time()
        g.request_date = datetime.now()

    @app.after_request
    def after_request(response):
        response_time = TimeHelper.calculate_response_time()
        current_app.logger.info('%s ms %s %s %s', response_time, request.method, request.path, dict(request.args))

        response.headers.add('X-Response-Time', str(response_time))

        return response
