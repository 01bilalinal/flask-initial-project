from flask import render_template, request

from src.context.success_resonse_context import SuccessResponseContext
from src.entity import Example
from src.helper.object_normalizer import Normalizer
from src.repository.example_repository import ExampleRepository
from src.request.example_request_schema import ExampleRequest


class IndexController:
    @staticmethod
    def index():
        return render_template('index.html')

    @staticmethod
    def create():
        example_repository = ExampleRepository()

        request_data = ExampleRequest.parse_raw(request.data)

        example = Example(
            username=request_data.username,
            first_name=request_data.first_name,
            last_name=request_data.last_name
        )
        example_repository.create(example)

        return Normalizer.to_json(
            SuccessResponseContext(
                message='Creation successful!',
                status_code=200,
                data=str(example)
            )
        )

    @staticmethod
    def find(username):
        example_repository = ExampleRepository()
        example = example_repository.find_by_username(username)

        return Normalizer.to_json(
            SuccessResponseContext(
                message='Find successful!',
                status_code=200,
                data=example.first_name
            )
        )
