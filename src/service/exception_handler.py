from flask import request, current_app
from pydantic import ValidationError
from werkzeug.exceptions import HTTPException

from src.context.error_resonse_context import ErrorResponseContext
from src.exception.abstract_exception import AbstractException
from src.helper.object_normalizer import Normalizer
from src.helper.time_helper import TimeHelper


class ExceptionHandler:
    CONTENT_TYPE = 'application/json'

    def __call__(self, error):
        response_time = TimeHelper.calculate_response_time()
        request_date = TimeHelper.get_request_date_isoformat()

        current_app.logger.info('Response Time %s -  Request Date %s', response_time, request_date)

        if request.content_type != ExceptionHandler.CONTENT_TYPE:
            raise error

        error_code: int = 500
        message = str
        errors: any = None

        if isinstance(error, Exception):
            error_code = 500
            message = str(error)

        if isinstance(error, (HTTPException, AbstractException)):
            error_code = error.code
            message = error.description

        if isinstance(error, ValidationError):
            errors = error.errors()
            error_code = 400
            message = 'Validation failed!'

        error_response = ErrorResponseContext(
            message=message,
            status_code=error_code,
            errors=errors
        )

        return Normalizer.to_json(error_response), error_response.status_code
