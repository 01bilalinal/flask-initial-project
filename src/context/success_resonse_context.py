from dataclasses import dataclass
from typing import Any

from src.context.abstract_response_context import AbstractResponseContext


@dataclass
class SuccessResponseContext(AbstractResponseContext):
    STATUS_OK = 'OK'

    data: Any = None
    status: str = STATUS_OK
