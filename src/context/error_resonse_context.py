from dataclasses import dataclass

from src.context.abstract_response_context import AbstractResponseContext


@dataclass
class ErrorResponseContext(AbstractResponseContext):
    STATUS_ERROR = 'ERROR'

    errors: list = None
    status: str = STATUS_ERROR
