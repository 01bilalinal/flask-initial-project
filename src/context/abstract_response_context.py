from dataclasses import dataclass


@dataclass
class AbstractResponseContext:
    status_code: int
    message: str
