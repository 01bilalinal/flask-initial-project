from abc import ABC, abstractmethod


class RepositoryInterface(ABC):
    @property
    @abstractmethod
    def _entity(self):
        pass

    @abstractmethod
    def create(self, entity):
        pass

    @abstractmethod
    def update(self, entity):
        pass

    @abstractmethod
    def delete(self, entity):
        pass

    @abstractmethod
    def find_by_id(self, entity_id):
        pass

    @abstractmethod
    def find_all(self):
        pass
