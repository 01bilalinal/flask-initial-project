import re

from pydantic import BaseModel, validator


class ExampleRequest(BaseModel):
    username: str
    first_name: str
    last_name: str

    @validator('username')
    def username_match(cls, v):
        regex = r"^(?=[a-zA-Z0-9._]{5,20}$)(?!.*[_.]{2})[^_.].*[^_.]$"
        match = re.match(regex, v)

        if not match:
            raise ValueError('Please enter a valid username!')

        return v
