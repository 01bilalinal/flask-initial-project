from flask import Blueprint as BP

from src.controller.index_controller import IndexController


class Blueprint:
    @staticmethod
    def api_routes():
        blueprint = BP('blueprint', __name__)

        blueprint.route('/', methods=['GET'])(IndexController.index)
        blueprint.route('/create', methods=['POST'])(IndexController.create)
        blueprint.route('/find/<username>', methods=['POST'])(IndexController.find)

        return blueprint
