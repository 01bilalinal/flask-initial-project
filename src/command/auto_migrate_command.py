import click
from flask.cli import with_appcontext

from extension.sqlalchemy import db
from src.interfaces.command_interface import CommandInterface


class AutoMigrateCommand(CommandInterface):
    @staticmethod
    @click.command('create-tables')
    @with_appcontext
    def execute():
        db.create_all()

        print('All table created!')
