import datetime
import json
from json import JSONEncoder


class Normalizer(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%d/%m/%Y %H:%M:%S')
        return obj.__dict__

    @staticmethod
    def to_json(obj: object):
        return json.loads(json.dumps(obj, cls=Normalizer))
