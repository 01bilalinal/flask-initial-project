import time

from flask import g


class TimeHelper:
    @staticmethod
    def calculate_response_time():
        return time.time() - g.start_time

    @staticmethod
    def get_request_date_isoformat():
        return g.request_date.isoformat()

    @staticmethod
    def get_request_date():
        return g.request_date
