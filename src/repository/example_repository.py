from src.entity.example import Example
from src.repository.abstract_repositroy import AbstractRepository


class ExampleRepository(AbstractRepository):
    _entity = Example

    def find_by_username(self, username) -> Example | None:
        return self._entity.query.filter_by(username=username).one_or_none()
