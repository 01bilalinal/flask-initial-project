from extension.sqlalchemy import db
from src.interfaces.repository_interface import RepositoryInterface


class AbstractRepository(RepositoryInterface):
    _entity: db.Model

    def create(self, entity):
        db.session.add(entity)
        db.session.commit()

        return entity

    def update(self, entity):
        db.session.commit()
        return entity

    def delete(self, entity):
        db.session.delete(entity)
        db.session.commit()

    def find_by_id(self, entity_id):
        return self._entity.query.get(entity_id)

    def find_all(self):
        return self._entity.query.all()
