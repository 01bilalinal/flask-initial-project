class AbstractException(Exception):
    code: int
    description: str
