from datetime import datetime

from extension.sqlalchemy import db


class Example(db.Model):
    __tablename__ = 'example'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(70), unique=True, nullable=False)
    first_name = db.Column(db.String(70), unique=True, nullable=False)
    last_name = db.Column(db.String(70), unique=True, nullable=False)
    created_at = db.Column(db.DateTime(), nullable=False, default=datetime.now())

    def __repr__(self) -> str:
        return f'{self.username!r}'
